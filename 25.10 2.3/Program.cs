﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecretAgent
{
    class MyUniqueList
    {
        List<int> list = new List<int>();

        public MyUniqueList()
        {

        }

        public bool Add(int item)
        {
            if (!list.Contains(item))
            {
                list.Add(item);
                return true;
            }

            return false;
        }

        public bool Remove(int item)
        {
            if (list.Contains(item))
            {
                list.Remove(item);
                return true;
            }
            else
            {
                throw new ItemNotFoundException("There is no such item in the list");
            }
        }

        public int Peek(int index)
        {
            if (index > list.Count)
                throw new IndexOutOfRangeException("Index out of range");
            return list[index];
        }

        public int this[int index]
        {
            get
            {
                return this.list[index];
            }
            set
            {
                if (list[index] == value)
                    return;
                if (list.Contains(value))              
                    throw new ItemAlreadyExistException("This number already exists in the list");                   
              
                list[index] = value;
            }
        }
    }
}
